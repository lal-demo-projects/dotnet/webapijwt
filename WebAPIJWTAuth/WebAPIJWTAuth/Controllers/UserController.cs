﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace WebAPIJWTAuth.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpPost]
        [AllowAnonymous]
        public string Login(string userName, string password)
        {
            string token = string.Empty;
            if (userName.ToLower() == "admin")
            {
                token = GenerateToken(userName, "Admin");
            }
            else if (userName.ToLower() == "manager")
            {
                token = GenerateToken(userName, "Manager");
            }
            else if (userName.ToLower() == "tester")
            {
                token = GenerateToken(userName, "Tester");
            }
            return token;
        }

        private string GenerateToken(string userName, string role)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("My secret key for token"));

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Email, userName + "@gmail.com"),
                new Claim(ClaimTypes.Role, role),
            };

            var token = new JwtSecurityToken("http://localhost:5271/", "http://localhost:5271/", claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
