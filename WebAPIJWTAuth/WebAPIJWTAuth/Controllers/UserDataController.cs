﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPIJWTAuth.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]/[action]")]
    public class UserDataController : ControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        public string GetGuestPackage()
        {
            return "Hello world";
        }

        [HttpGet]
        public string GetUserPackage()
        {
            return "Hello user";
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public string GetManagerPackage()
        {
            return "Hello manager";
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public string GetAdminPackage()
        {
            return "Hello admin";
        }
    }
}
